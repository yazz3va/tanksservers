package com.TGS.Profiles;

import com.TGS.DataBaseQuery.LoginTable;
import com.TGS.HandlerType.PacketHand;
import com.TGS.HandlerType.RegistrationTypes;
import com.TGS.HandlerType.Types;
import yazZ3va.RUCP.ClientSocket.Packet;
import yazZ3va.RUCP.Tools.HandlerPack;

public class Profile implements HandlerPack {
    private PlayerInfo player_info = null;
    private PacketHand[] types;


    public Profile() {
        player_info = new PlayerInfo();
        player_info.tank = new TankInfo();
        types = RegistrationTypes.getStaticTypes();
    }

    public PlayerInfo playerInfo(){
        return player_info;
    }

    public void registerType(short type, PacketHand func){
       types[type] = func;
    }

    @Override
    public boolean openConnection(Packet packet) {
        System.out.println("open connection");
        int id = packet.nextInt();
        int key = packet.nextInt();


        if (key == LoginTable.getSessionKey(id)) {
            player_info.id = id;
            player_info.sessionkey = key;
            player_info.client = packet.getClient();
            return true;
        }
            System.out.println("SSK not valid");
        return false;
    }

    @Override
    public void channelRead(Packet packet) {
        int type = (packet.getType() < 0 || packet.getType() >= types.length)? 0 : packet.getType();
        types[type].handle(this, packet);
    }

    @Override
    public void closeConnection() {
        if(types[Types.CloseConnection] != null) types[Types.CloseConnection].handle(this, null);

        player_info.tank = null;
        player_info.client = null;
        player_info = null;
    }

}