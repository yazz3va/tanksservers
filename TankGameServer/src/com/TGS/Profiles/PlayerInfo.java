package com.TGS.Profiles;

import yazZ3va.RUCP.ClientSocket.ClientSocket;

public class PlayerInfo {
    public int id;
    public int sessionkey;
    public TankInfo tank;
    public ClientSocket client;
    private int keyboard = 0;
    public int lastState = 0;
    public byte[] name;


    public PlayerInfo()
    {

        this.name = new byte[3];
    }
    public PlayerInfo(TankInfo pl, ClientSocket cl, int id)
    {
        tank = pl;
        client = cl;
        this.id = id;
    }

    public void setKeyboard(int keyboard) {
        this.keyboard = keyboard;
    }

    public int getKeyboard() {
        return keyboard;
    }
}
