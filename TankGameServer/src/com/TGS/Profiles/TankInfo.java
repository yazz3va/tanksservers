package com.TGS.Profiles;

//import org.dyn4j.dynamics.Body;
//import org.dyn4j.geometry.Vector2;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;

public class TankInfo {
    public boolean online;
    public int tankId;
    public int hp;
    public int maxHp;
    public Vec2 pos(){return body.getTransform().position; }
    public float rotation(){return body.getTransform().getAngle(); }
    public Body body;
}
