package com.TGS.DataBaseQuery;

import com.TGS.DataBase.ConnectionSet;
import com.TGS.DataBase.LDBHandler;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginTable {
    public static int getSessionKey(int id)
    {
        int key = 0;

            try(ConnectionSet connectionSet = LDBHandler.ExecuteReader("SELECT sessionkey FROM login_table WHERE idlogin=" + id + ";"))
            {
                ResultSet reader = connectionSet.getResultSet();
                if (reader.next())
                {
                    key = reader.getInt(1);
                }
            } catch (SQLException e){
                System.out.println("Get session key: "+e);
            }

        return key;
    }
}
