package com.TGS.HandlerType;

import com.TGS.BattleRoom.RoomFind;
import com.TGS.Lobby.LobbyEntrance;
import com.TGS.Profiles.Profile;
import yazZ3va.RUCP.ClientSocket.Packet;

public class RegistrationTypes {

    private static PacketHand[] handlers;

    public static void AllRegister(){
        handlers = new PacketHand[50];

        handlers[0] = new PacketHand() {
            @Override
            public void handle(Profile client, Packet packet) {
                System.out.println("unknown type: "+packet.getType());
            }
        };

      //  handlers[Types.Login] = Login::logIn;
        handlers[Types.LobbyEntrance] = LobbyEntrance::entrance;
        handlers[Types.RoomFind] = RoomFind::find;


    }

    public static PacketHand[] getStaticTypes(){
        return handlers.clone();
    }
}


