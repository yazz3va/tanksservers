package com.TGS.HandlerType;

public class Types {
    public static final short CloseConnection = 1;// S->S Метод выполняемы во время отключения игрока
    public static final short LobbyEntrance = 2; //s->c Вход игрока в общую комнату
    public static final short MyCharacters = 3; //C->S запрос на получение списка персонажей, S->C Список персонажей
    public static final short OwnCharacterCreate = 4; //C->S->C Отправка на сервер запроса на создание персонажа и отпрака обратно подтверждение или ошибки
    public static final short SetCharacter = 5; //S->C Выбор персонажа
    public static final short MyCCharacter = 6; //C->S Запрос на получение информации о выбраном персонаже S->C Информация о выбраном персонаже
    public static final short RoomFind = 7; // S->C Запрос на поиск комнаты // C->S
    public static final short Login = 8;  //C->S отпровляет логин и пароль на сервер
    public static final short Registration = 9; //C->S отпровляет логин и пароль  для регистрации акаунта
    public static final short LoginInfo = 10; //S->C  Отпровляет код ошибки или успешного выполнение запроса
    public static final short LoginOk = 11; //S->C Логин успешен
    public static final short ServersList = 12; //C->S запрос на получение списка серверов S->C Список серверов
    public static final short Move = 13;  //C->S Передвижение персонажа на карте
    public static final short Charactermove = 14; //S->C Передвижение других игроков
    public static final short RoomEntrance = 15; //С->S На клиенте загрузилась комната, запрос на получение данных
    public static final short CharacterCreate = 16; // S->C данные для создание других игроков
    public static final short KeyControl = 17;//C->S Запрос на взятие цели по ид. S->C ответ о возможности взять в цель
}
