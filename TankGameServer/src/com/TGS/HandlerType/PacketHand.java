package com.TGS.HandlerType;

import com.TGS.Profiles.Profile;
import yazZ3va.RUCP.ClientSocket.Packet;

public interface PacketHand{
    void handle(Profile client, Packet packet);
}
