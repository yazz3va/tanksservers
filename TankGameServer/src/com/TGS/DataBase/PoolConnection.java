package com.TGS.DataBase;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class PoolConnection {

    private ComboPooledDataSource cpds;

   public PoolConnection(String server, String user, String password){
        cpds = new ComboPooledDataSource();
        try {
            cpds.setDriverClass("com.mysql.jdbc.Driver"             );
            cpds.setJdbcUrl    ("jdbc:mysql://"+server+"?autoReconnect=true&useSSL=false");
            cpds.setUser       (user   );
            cpds.setPassword   (password);

            Properties properties = new Properties();
            properties.setProperty ("user"             , user   );
            properties.setProperty ("password"         , password);
            properties.setProperty ("useUnicode"       , "true"      );
            properties.setProperty ("characterEncoding", "UTF8"      );
            cpds.setProperties(properties);

            // set options
            cpds.setMaxStatements             (180);
            cpds.setMaxStatementsPerConnection(180);
            cpds.setMinPoolSize               ( 3);
            cpds.setAcquireIncrement          ( 10);
            cpds.setMaxPoolSize               ( 10);
            cpds.setMaxIdleTime               ( 30);
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection(){
        try {
            return cpds.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
