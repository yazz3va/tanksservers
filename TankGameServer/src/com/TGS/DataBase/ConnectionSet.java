package com.TGS.DataBase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionSet implements AutoCloseable {

    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;

    public ConnectionSet(Connection connection)
    {
       this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    @Override
    public void close()
    {

        try {
           if(resultSet != null)  resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        try {
          if(statement != null)  statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        try {
          if(connection != null)  connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
