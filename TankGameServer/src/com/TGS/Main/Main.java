package com.TGS.Main;

import com.TGS.DataBase.LDBHandler;
import com.TGS.Profiles.Profile;
import com.TGS.HandlerType.RegistrationTypes;
import yazZ3va.RUCP.Server.Handler;
import yazZ3va.RUCP.Server.Server;
import yazZ3va.RUCP.Tools.HandlerPack;

public class Main {
    public static void main(String[] args){
        System.out.println("Tanks Game Server ver. 0.02a");
        LDBHandler.connection();
        RegistrationTypes.AllRegister();

        Server server = new Server(3232);
        Server.setHandler(new Handler() {
            @Override
            public HandlerPack getHandler() {
                return new Profile();
            }
        });

        server.start();


    }
}
