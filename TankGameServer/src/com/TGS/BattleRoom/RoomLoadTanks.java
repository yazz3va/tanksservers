package com.TGS.BattleRoom;

import com.TGS.HandlerType.Types;
import com.TGS.Profiles.PlayerInfo;
import yazZ3va.RUCP.ClientSocket.Packet;
import yazZ3va.RUCP.Sender.Sender;
import yazZ3va.RUCP.Server.Channel;

public class RoomLoadTanks {
    public static void load(PlayerInfo[] players, PlayerInfo player)
    {


        synchronized (players)
        {
            System.out.println("Load : "+player.name);
            for (int i = 0; i < players.length; i++)
            {
                if (players[i] == null) continue;
                if (players[i].id != player.id && players[i].tank.online)
                {
                    //Отправка информации о уже зашедших игроках новому игроку
                    Packet packet = new Packet(player.client, Channel.Reliable, 17 + players[i].name.length);
                    packet.writeType(Types.CharacterCreate);
                    packet.writeInt(players[i].id);
                    packet.writeInt(players[i].tank.tankId);
                    packet.writeFloat((float)players[i].tank.pos().x);
                    packet.writeFloat((float)players[i].tank.pos().y);
                    packet.writeByte((byte)players[i].name.length);
                    packet.writeBytes(players[i].name);
                    Sender.send(packet);//<<<

                    //Отправка информации о новом игроке игракам которые уже в игре
                    packet = new Packet(players[i].client, Channel.Reliable, 17 + player.name.length);
                    packet.writeType(Types.CharacterCreate);
                    packet.writeInt(player.id);
                    packet.writeInt(player.tank.tankId);
                    packet.writeFloat((float)player.tank.pos().x);
                    packet.writeFloat((float)player.tank.pos().y);
                    packet.writeByte((byte)player.name.length);
                    packet.writeBytes(player.name);
                    Sender.send(packet);//<<<
                }


            }
            player.tank.online = true;
        }

        //Отправка информации о своем танке
        Packet packet = new Packet(player.client, Channel.Reliable, 17 + player.name.length);
        packet.writeType(Types.CharacterCreate);
        packet.writeInt(player.id);
        packet.writeInt(player.tank.tankId);
        packet.writeFloat((float)player.tank.pos().x);
        packet.writeFloat((float)player.tank.pos().y);
        packet.writeByte((byte)player.name.length);
        packet.writeBytes(player.name);
        Sender.send(packet);//<<<

    }


    public static boolean destroy(PlayerInfo[] players, PlayerInfo player)
    {
        for (int i = 0; i < players.length; i++)
        {
            if (players[i] == null) continue;
            if (players[i].id == player.id)
            {
                players[i] = null;
                return true;
            }
        }
        return false;
    }
}
