package com.TGS.BattleRoom;

import com.TGS.HandlerType.Types;
import com.TGS.Profiles.PlayerInfo;
import com.TGS.Profiles.Profile;
import yazZ3va.RUCP.ClientSocket.ClientSocket;
import yazZ3va.RUCP.ClientSocket.Packet;
import yazZ3va.RUCP.Sender.Sender;
import yazZ3va.RUCP.Server.Channel;

public class RoomFind {
    public static void find(Profile client, Packet packet)
    {
        System.out.println("Room find");
        if (client.playerInfo().tank.tankId < 0) { SendAnswer(packet.getClient(),2); return; }


        Room room = RoomFindList.getRoomRank(0);//Поиск комнаты подходящую по рангу
        if (room == null)
        {//Если команата не найдена создаем новую
            room = RoomFindList.createRoomRank(0);
        }

        if (room.ConnectToRoom(client))//Если подключение удалось
        {
            SendAnswer(packet.getClient(), 1);
            return;
        }

        SendAnswer(packet.getClient(), 3);
    }

    //1- Успешное подключение
    //2- Танк не выбран
    //3- Комната переполнена повторите попытку
    private static void SendAnswer(ClientSocket client, int id_error)
    {
        Packet packet = new Packet(client, Channel.Reliable, 4);
        packet.writeType(Types.RoomFind);
        packet.writeByte((byte)id_error);
        Sender.send(packet);
    }
}
