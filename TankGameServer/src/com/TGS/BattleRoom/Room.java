package com.TGS.BattleRoom;

import com.TGS.HandlerType.Types;
import com.TGS.Physics.Map;
import com.TGS.Profiles.PlayerInfo;
import com.TGS.Profiles.Profile;
import yazZ3va.RUCP.ClientSocket.Packet;

public class Room {
    private PlayerInfo[] players;
    private Map map;
    private int online = 0;

    public Room()
    {
        players = new PlayerInfo[8];
        map = new Map(players);
    }

    public boolean ConnectToRoom(Profile player)
    {
        boolean connection = false;
        synchronized (players)
        {
            for (int i = 0; i < players.length; i++)
            {
                if (players[i] == null)
                {
                    players[i] = player.playerInfo();
                    connection = true;
                    break;
                }
            }
        }
        if (connection)
        {
            RegisterTypes(player);
            online++;
        }
        else
        {
            RoomFindList.removeRoomRank(0);
        }
        return connection;
    }

    private void RegisterTypes(Profile player)
    {
       // player.registerType(Types.Move, this::move);
        player.registerType(Types.RoomEntrance, this::roomEntrance);
        player.registerType(Types.CloseConnection, this::roomExit);
        player.registerType(Types.KeyControl, this::keyControl);
    }

    private void keyControl(Profile client, Packet packet)
    {
       int diff = packet.nextByte() - client.playerInfo().lastState;
       if(diff > 0 || diff < -90) client.playerInfo().setKeyboard(packet.nextByte());
    }

    //Отключение игрока от комнаты
    private void roomExit(Profile client, Packet packet)
    {
        if(RoomLoadTanks.destroy(players, client.playerInfo()))online--;

        if (online <= 0)//Если не осталось игроков удаляем комнату
        {
            RoomFindList.removeRoomRank(0);
         //   map.Off();//Отключение игрового мира
        }

    }
    private void roomEntrance(Profile client, Packet packet)
    {
        //Создание танка на карте
        client.playerInfo().tank.body = map.createTank();
        //Рассылка информации игроков
                RoomLoadTanks.load(players, client.playerInfo());
    }

  /*  public void move(Profile client, Packet packet)
    {
        TankMove.Move(players, client.playerInfo(), packet);
    }*/
}
