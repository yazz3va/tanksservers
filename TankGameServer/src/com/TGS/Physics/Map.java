package com.TGS.Physics;


import com.TGS.Profiles.PlayerInfo;
/*import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.dynamics.World;
import org.dyn4j.dynamics.joint.MotorJoint;
import org.dyn4j.geometry.Convex;
import org.dyn4j.geometry.Geometry;
import org.dyn4j.geometry.MassType;
import org.dyn4j.geometry.Vector2;
import com.TGS.Profiles.Tank;*/
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.*;
import org.jbox2d.pooling.normal.DefaultWorldPool;

public class Map implements Runnable {
    private boolean game = true;
    private World world;
    private PlayerInfo[] players;
    private int cicle = 0, maxCicle = 20;

    // Prepare for simulation. Typically we use a time step of 1/60 of a
    // second (60Hz) and 10 iterations. This provides a high quality simulation
    // in most game scenarios.
    private final float timeStep = 0.008f;
    private final int timeSleep = 8;


    public Map(PlayerInfo[] players)
    {
        this.players = players;
        Thread th = new Thread(this);
        th.start();
    }

    public void run()
    {
        start();
        while (game) {
            input();
            update();
            if(cicle++ >= maxCicle){ TankMove.Move(players); cicle = 0;}
            try {
                Thread.sleep(timeSleep);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void input()
    {
        for(PlayerInfo player : players){
            if(player == null || !player.tank.online) continue;
            int key = player.getKeyboard();

          //  Vector2 velocity = new Vector2(0,0);
            Vec2 direction = new Vec2(0,1);
            float velocity = 0.0f;
            float rotation = 0;

            if((key & KeyCode.W) != 0){
              velocity += 2.0f;
            }
            if((key & KeyCode.S) != 0){
                velocity -= 2.0f;
            }
            if((key & KeyCode.A) != 0){
                rotation += 1.0;
            }
            if((key & KeyCode.D) != 0){
                rotation -= 1.0;
            }

            player.tank.body.setAngularVelocity(rotation);
            rotation = player.tank.rotation();
            Vector2.setDirection(direction, rotation).normalize();
            player.tank.body.setLinearVelocity(direction.mul(velocity));
        }
    }


    private void start()
    {
        world = createWorld();
    }

    private void update()
    {
        // Instruct the world to perform a single step of simulation. It is
        // generally best to keep the time step and iterations fixed.
        world.step(timeStep, 1,1);
    }

    public void Off()
    {
        game = false;
    }

    private World createWorld()
    {
        Vec2  gravity = new Vec2(0,0);
        World world = new World(gravity, true, new DefaultWorldPool(200,200));

      //  World world = new World();
      //  world.setGravity(World.ZERO_GRAVITY);

        return world;
    }

    public Body createTank()
    {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.DYNAMIC;
        bodyDef.position.set(0, 4);
        Body body = world.createBody(bodyDef);
        PolygonShape dynamicBox = new PolygonShape();
        dynamicBox.setAsBox(2.15f, 1.4f);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = dynamicBox;
        fixtureDef.density = 1;
        fixtureDef.friction = 0.8f;
        body.createFixture(fixtureDef);

        return body;
      /*  Body body = new Body();
        body.addFixture(Geometry.createRectangle(4.3, 2.8));
        body.translate(new Vector2(4.3, 2.8));
        body.setMass(MassType.NORMAL);
        body.setAutoSleepingEnabled(false);
        this.world.addBody(body);


        // Controller
        Body controller = new Body();
        {// Fixture1
            Convex c = Geometry.createRectangle(4.3, 2.8);
            BodyFixture bf = new BodyFixture(c);
            // make sure the controller body doesn't participate in
            // collision.  A better way would go the route of using
            // collision filters, but doing this made the sample smaller.
            bf.setSensor(true);
            controller.addFixture(bf);
        }

        controller.translate(new Vector2(4.3, 2.8));
        controller.setAngularVelocity(0.0);
        // make sure the controller body cannot be moved by anything
        // even though the body is mass type infinite, you can still
        // move the body around manually by modifying its transform.
        // This is what we do below
        controller.setMass(MassType.INFINITE);
        world.addBody(controller);

        // MotorJoint2
        MotorJoint joint1 = new MotorJoint(controller, body);
        joint1.setLinearTarget(new Vector2(0.0, 0.0));
        joint1.setAngularTarget(Math.toRadians(0.0));
        joint1.setCorrectionFactor(1.0);
        // we dont want the motor joint to move the player to the controller
        // we just want it to rotate it.  So we turn that feature off by setting
        // the maximum force to 0.0.  In other words, no force will ever be applied
        // to the bodies to move them.
        joint1.setMaximumForce(1000.0);
        // allow rotational changes (change this depending on how fast you want
        // the player body to react to changes in the controller body)
        joint1.setMaximumTorque(1000.0);
        joint1.setCollisionAllowed(false);
        world.addJoint(joint1);

        return new Tank(body, controller);*/
    }

    public void removeTank(Body body){
        world.destroyBody(body);
    }
}
