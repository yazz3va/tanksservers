package com.TGS.Physics;

import com.TGS.HandlerType.Types;
import com.TGS.Profiles.PlayerInfo;
import yazZ3va.RUCP.ClientSocket.Packet;
import yazZ3va.RUCP.Sender.Sender;
import yazZ3va.RUCP.Server.Channel;

public class TankMove {
    public static void Move(PlayerInfo[] players)
    {
        for(PlayerInfo player : players) {
                        if(player == null || !player.tank.online) continue;
            for (int i = 0; i < players.length; i++) {
                if (players[i] == null) continue;
                if (!players[i].tank.online) continue;
                    Packet packet = new Packet(players[i].client, Channel.Reliable, 16);
                    packet.writeType(Types.Charactermove);
                    packet.writeInt(player.id);
                    packet.writeFloat((float)player.tank.pos().x);
                    packet.writeFloat((float)player.tank.pos().y);
                    packet.writeFloat(player.tank.rotation());
                    Sender.send(packet);

            }
        }
    }
}
