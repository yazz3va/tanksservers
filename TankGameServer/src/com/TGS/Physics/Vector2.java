package com.TGS.Physics;

import org.jbox2d.common.Vec2;

public class Vector2 {

    public static Vec2 setDirection(Vec2 vec, float angle){
        double magnitude =  Math.sqrt(vec.x * vec.x + vec.y * vec.y);
        vec.x = (float) (magnitude * Math.cos(angle));
        vec.y = (float) (magnitude * Math.sin(angle));
        return vec;
    }
}
