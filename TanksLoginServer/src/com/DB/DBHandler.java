package com.DB;




import java.sql.*;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DBHandler {

   private static String server = "localhost:3306/tanksldb";
   private static String user = "yazZ3va";
   private static String password = "5475269qaz";


    //Переменная Connect - это строка подключения в которой:
    //БАЗА - Имя базы в MySQL
    //ХОСТ - Имя или IP-адрес сервера (если локально то можно и localhost)
    //ПОЛЬЗОВАТЕЛЬ - Имя пользователя MySQL
    //ПАРОЛЬ - говорит само за себя - пароль пользователя БД MySQL
    private static PoolConnection poolConnection;



    public static ConnectionSet ExecuteReader(String CommandText)
    {

        ConnectionSet connectionSet = new ConnectionSet(poolConnection.getConnection());
        try
        {
            connectionSet.setStatement(
                      connectionSet.getConnection().createStatement()
            );

            // executing SELECT query
           connectionSet.setResultSet(
                           connectionSet.getStatement().executeQuery(CommandText)
           );

        }

        catch (SQLException e)
        {
           System.out.println("error ExecuteReader: " + e);
        }
        return connectionSet;
    }



    public static void ExecuteNonQuery(String CommandText)
    {

        try(ConnectionSet connectionSet = new ConnectionSet(poolConnection.getConnection()))
        {
            connectionSet.setStatement(
                    connectionSet.getConnection().createStatement()
            );

            connectionSet.getStatement().executeUpdate(CommandText);
        }
        catch (SQLException e)
        {
            System.out.println("Error ExecuteNonQuery: " + e);
        }

    }


    public static void connection() {


        poolConnection = new PoolConnection(server, user, password);
    }
}
