package com.Tanks.Login;

import yazZ3va.RUCP.ClientSocket.Packet;
import yazZ3va.RUCP.Tools.HandlerPack;

public class Client implements HandlerPack {
    @Override
    public boolean openConnection(Packet packet) {
        return true;
    }

    @Override
    public void channelRead(Packet packet) {
        RegistrationPacket.get(packet.getType()).handle(this, packet);
    }

    @Override
    public void closeConnection() {

    }
}
