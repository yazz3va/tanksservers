package com.Tanks.Login;

public class Types {
    public static final int name = 1;// С->S отпровляет запрос серверу на создание профиля игрока с именем
    public static final int selfCharacterCreate = 2; //s->c Информация для создания своего персонажа
    public static final int CharacterCreate = 3; //S-C данные для создания обьекта персонажа на клиенте
    public static final int PlayerMove = 4; //C->S->C Отправка на сервер нового положение игрока и отпрака обратно подтвержденного положение
    public static final int CharacterMove = 5; //S->C Отправка на клиент положения персонажей
    public static final int CharacterRemove = 6; //S->C Удаляет обьект персонажа на клиенте
    public static final int selfCharacterRemove = 7; // S->C Выход из карты
    public static final int Login = 8;  //C->S отпровляет логин и пароль на сервер
    public static final int Registration = 9; //C->S отпровляет логин и пароль  для регистрации акаунта
    public static final int LoginInfo = 10; //S->C  Отпровляет код ошибки или успешного выполнение запроса
    public static final int LoginOk = 11; //S->C Логин успешен
    public static final int ServersList = 12; //C->S запрос на получение списка серверов S->C Список серверов
}
