package com.Tanks.Login;

import com.Query.Login;
import com.Query.Registration;
import com.Servers.ServerList;
import yazZ3va.RUCP.ClientSocket.Packet;

public class RegistrationPacket {

    private static PacketHand[] handlers;

    public static void AllRegister(){
        handlers = new PacketHand[50];

        handlers[0] = new PacketHand() {
            @Override
            public void handle(Client client, Packet packet) {
                  System.out.println("unknown type: "+packet.getType());
            }
        };

        handlers[Types.Login] = Login::logIn;
        handlers[Types.Registration] = Registration::register;
        handlers[Types.ServersList] = ServerList::getList;

    }

    public static PacketHand get(int type){
        if(type < 0 || type >= handlers.length || handlers[type] == null) return handlers[0];
         return handlers[type];
    }
}

interface PacketHand{
    void handle(Client client, Packet packet);
}
