package com.Tanks.Login;


import com.DB.DBHandler;
import com.Servers.ServerReader;
import yazZ3va.RUCP.Server.Handler;
import yazZ3va.RUCP.Server.Server;
import yazZ3va.RUCP.Tools.HandlerPack;

public class Main {

    public static void main(String[] args){
        System.out.println("Tanks Login Server ver. 0.03a");
        DBHandler.connection();
        RegistrationPacket.AllRegister();
        ServerReader.load();
        Server server = new Server(3737);
        Server.setHandler(new Handler() {
            @Override
            public HandlerPack getHandler() {
                return new Client();
            }
        });

        server.start();


    }
}


