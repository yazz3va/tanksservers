package com.Query;

import com.DBQuery.LoginTable;
import com.Tanks.Login.Client;
import com.Tanks.Login.Types;
import com.Tools.Rand;
import yazZ3va.RUCP.ClientSocket.ClientSocket;
import yazZ3va.RUCP.ClientSocket.Packet;
import yazZ3va.RUCP.Sender.Sender;
import yazZ3va.RUCP.Server.Channel;


import java.nio.charset.StandardCharsets;

public class Login {
    public static final short version = 1;
    public static void logIn(Client info, Packet packet)
    {
        if (packet.nextShort() != version) { Answers.sendInfo(packet.getClient(), 6); return; }//Версия клиента не соответствует серверу



        String login = new String(packet.nextBytes(packet.nextByte()), StandardCharsets.US_ASCII);

        if (login.length() < 3 || login.length() > 30)
        {
            Answers.sendInfo(packet.getClient(), 2);//Логин не должен содержать меньше 3 или больше 30 символов
            return;
        }

        String password = new String(packet.nextBytes(packet.nextByte()), StandardCharsets.US_ASCII);

            /*  if (password.Length < 3 || password.Length > 30)
              {
                  Registration.SendInfo(pack, 3);//пароль не должен содержать меньше 3 или больше 30 символов
                  return;
              }*/

        //Запрос есть ли данный логин в БД
        String db_pass = LoginTable.getPassword(login);

        if (db_pass != null && db_pass.equals(password))
        {
            int sessionkey = Rand.nextInt();
            LoginTable.SetSessionKey(login, sessionkey);
            sendSessionKey(packet.getClient(), LoginTable.GetID(login), sessionkey);
        }
        else
        {
            Answers.sendInfo(packet.getClient(), 7);//Логин или пароль неверны
        }

    }

    private static void sendSessionKey(ClientSocket client, int id, int key)
    {
        Packet packet = new Packet(client, Channel.Reliable, 8);
        packet.writeType(Types.LoginOk);
        packet.writeInt(id);
        packet.writeInt(key);
        Sender.send(packet);
    }
}
