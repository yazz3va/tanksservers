package com.Query;

import com.Tanks.Login.Types;
import yazZ3va.RUCP.ClientSocket.ClientSocket;
import yazZ3va.RUCP.ClientSocket.Packet;
import yazZ3va.RUCP.Sender.Sender;
import yazZ3va.RUCP.Server.Channel;

public class Answers {

    public static void sendInfo(ClientSocket client, int code)
    {
        Packet packet = new Packet(client, Channel.Reliable, 1);

        packet.writeType(Types.LoginInfo);
        packet.writeByte((byte)code);
        Sender.send(packet);
    }
}
