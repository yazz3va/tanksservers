package com.Query;

import com.DBQuery.LoginTable;
import com.Tanks.Login.Client;
import yazZ3va.RUCP.ClientSocket.Packet;

import java.nio.charset.StandardCharsets;

public class Registration {

    public static void register(Client client, Packet packet)
    {
        if (packet.nextShort() != Login.version) { Answers.sendInfo(packet.getClient(), 6); return; }//Версия клиента не соответствует серверу


        String login = new String(packet.nextBytes(packet.nextByte()), StandardCharsets.US_ASCII);

        if (login.length() < 3 || login.length() > 30)
        {
            Answers.sendInfo(packet.getClient(), 2);//Логин не должен содержать меньше 3 или больше 30 символов
            return;
        }

        String password = new String(packet.nextBytes(packet.nextByte()), StandardCharsets.US_ASCII);


        //Запрос есть ли данный логин в БД, если есть выход с отправкой на клиент кодом ошибки
        if (LoginTable.IsLogin(login))
            Answers.sendInfo(packet.getClient(), 4);//Такой логин уже используется


        LoginTable.CreateColumn(login, password);

        Answers.sendInfo(packet.getClient(), 5); //Регистрация успешна


    }
}
