package com.Servers;

public class ServerInfo {

    private String name = null;
    private String ip = null;
    private int port = 0;

    public String name()
    {
            return name;
    }
    public String ip()
    {
            return ip;
    }
    public int port()
    {
            return port;
    }

    public boolean setNext(String next)
    {
        if (name == null)
        {
            name = next;
            return false;
        }
        else if (ip == null)
        {
            ip = next;
            return false;
        }
        else
        {
            port = Integer.parseInt(next);
            System.out.println(name+" < "+ip+" < "+port);
            return true;
        }
    }
}
