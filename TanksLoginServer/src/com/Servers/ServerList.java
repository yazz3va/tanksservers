package com.Servers;

import com.Tanks.Login.Client;
import com.Tanks.Login.Types;
import yazZ3va.RUCP.ClientSocket.Packet;
import yazZ3va.RUCP.Sender.Sender;
import yazZ3va.RUCP.Server.Channel;

public class ServerList {
    public static void getList(Client client, Packet pack)
    {

       Packet packet = new Packet(pack.getClient(), Channel.Reliable, ServerReader.length());
        packet.writeType(Types.ServersList);
        for (ServerInfo server : ServerReader.getList())
        {
            packet.writeByte((byte)server.name().length());
            packet.writeBytes(server.name().getBytes());
            packet.writeByte((byte)server.ip().length());
            packet.writeBytes(server.ip().getBytes());
            packet.writeInt(server.port());
        }
        Sender.send(packet);
    }
}
