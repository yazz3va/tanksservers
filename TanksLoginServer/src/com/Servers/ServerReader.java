package com.Servers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class ServerReader {
    private static final String file = "servers.txt";
    private static ArrayList<ServerInfo> servers = new ArrayList<>(10);
    private static int servers_length = 0;

    public static int length()
    {
            return servers_length;
    }

    public static ArrayList<ServerInfo> getList()
    {
            return servers;
    }

    public static void load()
    {

            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {

                String word = "";
                ServerInfo server = new ServerInfo();
                int symbol = bufferedReader.read();
                while (symbol != -1) {
                    char ch = (char) symbol;

                    if (ch == ':' || ch == ';') {
                        if (server.setNext(word)) {
                            servers.add(server);
                            server = new ServerInfo();
                        }
                        word = "";
                    } else {
                        if (!Character.isWhitespace(ch)) word += ch;
                    }
                 symbol = bufferedReader.read();
                }
                servers_length = 0;

                for (ServerInfo ser : servers) {
                    servers_length++;
                    servers_length += ser.name().length();
                    servers_length++;
                    servers_length += ser.ip().length();
                    servers_length += 4;
                }
            }
        catch (IOException e)
        {
            System.out.println("Не удалось загрузить сервера");
            System.out.println(e);
        }
    }
}
